from django import forms
from django.forms import TextInput

from beekeepers.models import Beekeeper


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Beekeeper
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please insert new name', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('name')
        all_beekeepers = Beekeeper.objects.all()
        for beekeeper in all_beekeepers:
            if beekeeper.name == name:
                msg = 'This category already exists saved in the database'
                self._errors['name'] = self.error_class([msg])
