from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView

from cart.models import Cart
from order.forms import OrderForm
from order.models import Order


class OrderCreateView(CreateView):
    template_name = 'order/order_create.html'
    model = Order
    success_url = reverse_lazy('create_new_order')
    form_class = OrderForm

    def form_valid(self, form):
        if form.is_valid() and not form.errors:
            order = form.save()
            list_with_products = []
            for products in Cart.object.filter(user_id=self.request.user.id):
                list_with_products.append(products.products)
            order.products_list = list_with_products
            order.save()
            Cart.object.filter(user_id=self.request.user.id).delete()
        return redirect('home')


class OrderListView(LoginRequiredMixin, ListView):
    template_name = 'order/order_list.html'
    model = Order
    context_object_name = 'all_orders'
