from django.contrib.auth.forms import UserCreationForm
from users.models import ExtendUser
from django.forms import TextInput


class ExtendUserCreateForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['username', 'first_name', 'last_name', 'phone', 'email']
        widgets = {
            'username': TextInput(attrs={'placeholder': 'Please insert new Username', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'Please insert new First Name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert new Last Name', 'class': 'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'Please insert new Phone Number', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please insert new Email Address', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(ExtendUserCreateForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
