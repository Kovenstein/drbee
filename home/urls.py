from django.urls import path

from home import views
from home.views import HomeTemplateView


urlpatterns = [
    path('', HomeTemplateView.as_view(), name='home'),
    path('counter/', views.counter, name='counter'),
    path('list_of_products/<int:pk>/', views.get_products_per_category, name='products_per_category')
]
