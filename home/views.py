from django.shortcuts import render
from django.views.generic import TemplateView

from product.models import Product


class HomeTemplateView(TemplateView):
    template_name = 'home/home_page.html'


def counter(request):
    visit_count = request.session.get('counter', 0)
    request.session['counter'] = visit_count + 1
    context = {
        'counter': visit_count
    }
    return render(request, 'home/counter.html', context)


def get_products_per_category(request, pk):
    all_products = Product.objects.filter(category_id=pk)
    context = {
        'all_products_per_category': all_products
    }
    return render(request, 'home/home_page.html', context)
