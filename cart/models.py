from django.contrib.auth.models import User
from django.db import models

from product.models import Product


class Cart(models.Model):
    object = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='a')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='b')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
