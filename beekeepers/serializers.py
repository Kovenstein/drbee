from rest_framework import serializers

from beekeepers.models import Beekeeper


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Beekeeper
        fields = ['name']
