from django.db import models


class Order(models.Model):
    first_name = models.CharField(max_length=50, null=False)
    last_name = models.CharField(max_length=50, null=False)
    address = models.CharField(max_length=50, null=False)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    products_list = models.CharField(max_length=30)
