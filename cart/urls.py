from django.urls import path

from cart import views

urlpatterns = [
    path('cart_add_product/<int:pk>/', views.cart, name='add_to_cart'),
    path('list_products_cart/', views.get_product_from_cart, name='list_products_cart'),
    path('cart_delete_product/<int:pk>/', views.CartDeleteView.as_view(), name='delete_product_cart'),
]
