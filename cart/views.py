from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from cart.models import Cart
from product.models import Product


def cart(request, pk):
    Cart.objects.create(user_id=request.user.id, product_id=pk)
    category_id = Product.objects.get(id=pk).category_id
    return redirect('products_per_category', category_id)


def get_product_from_cart(request):
    return render(request, 'cart/cart_add_product.html', {'all_products': Cart.objects.filter(user_id=request.user.id)})


class CartDeleteView(DeleteView):
    template_name = 'cart/cart_delete_product.html'
    model = Cart
    context_object_name = 'list_products_cart'
    success_url = reverse_lazy('list_products_cart')
