from django.apps import AppConfig


class BeekeepersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'beekeepers'
