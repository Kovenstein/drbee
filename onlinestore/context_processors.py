from cart.models import Cart
from category.models import Category


def get_all_categories(_):
    all_categories = Category.objects.all()
    return {'all_categories': all_categories}


def counter_product_to_cart(request):
    all_products_counter = Cart.objects.filter(user_id=request.user.id).count()
    return {'count': all_products_counter}
