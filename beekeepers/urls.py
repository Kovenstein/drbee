from django.urls import path, include
from rest_framework import routers

from beekeepers import views
from beekeepers.views import CategoryViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)
urlpatterns = [
    path('create_beekeeper/', views.BeekeeperCreateView.as_view(), name='create_new_beekeeper'),
    path('list_of_beekeepers/', views.BeekeeperListView.as_view(), name='all_beekeepers'),
    path('update_beekeeper/<int:pk>', views.BeekeeperUpdateView.as_view(), name='update-beekeeper'),
    path('delete_beekeeper/<int:pk>', views.BeekeeperDeleteView.as_view(), name='delete-beekeeper'),
    path('details_beekeeper/<int:pk>', views.BeekeeperDetailsView.as_view(), name='detail-beekeeper'),
    path('api/', include(router.urls)),
    path('rest-api/', include('rest_framework.urls', namespace='rest_framework')),
]
