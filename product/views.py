from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, DeleteView, UpdateView, ListView, CreateView
from rest_framework import viewsets

from product.forms import CategoryForm
from product.models import Product
from product.serializer import CategorySerializer


def products(request):
    return render(request, 'index_second.html')


class ProductCreateView(CreateView):
    template_name = 'product/product_create.html'
    model = Product
    # fields = '__all__'
    success_url = reverse_lazy('create_new_product')
    form_class = CategoryForm


class ProductListView(ListView):
    template_name = 'product/product_list.html'
    model = Product
    context_object_name = 'all_products'


class ProductUpdateView(UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    context_object_name = 'all_Products'
    fields = ['name', 'type', 'weight']
    success_url = reverse_lazy('all_products')
    form_class = CategoryForm


class ProductDeleteView(DeleteView):
    template_name = 'product/product_delete.html'
    model = Product
    context_object_name = 'product'
    success_url = reverse_lazy('all_products')


class ProductDetailsView(DetailView):
    template_name = 'product/product_details.html'
    model = Product
    context_object_name = 'product'


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = CategorySerializer
