from django import forms
from django.forms import TextInput

from product.models import Product


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please insert new name', 'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Please insert price', 'class': 'form-control'}),
            'quantity': TextInput(attrs={'placeholder': 'Please insert quantity', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('name')
        all_products = Product.objects.all()
        for product in all_products:
            if product.name == name:
                msg = 'This category already exists saved in the database'
                self._errors['name'] = self.error_class([msg])
