from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    weight = models.IntegerField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def filter(self, user_id):
        pass

    def create(self, user_id, product_id):
        pass
