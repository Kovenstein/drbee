from django.urls import path, include
from rest_framework import routers

from product import views
from product.views import CategoryViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)

urlpatterns = [
    path('add_product/', views.ProductCreateView.as_view(), name='create_new_product'),
    path('update_product/', views.ProductUpdateView.as_view(), name='update-product'),
    path('list_of_products/', views.ProductListView.as_view(), name='all_products'),
    path('delete_product/', views.ProductDeleteView.as_view(), name='delete-product'),
    path('details_product/', views.ProductDetailsView.as_view(), name='detail-product'),
    path('api/', include(router.urls)),
    path('rest-api/', include('rest_framework.urls', namespace='rest_framework')),

]
