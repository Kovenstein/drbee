from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from users.forms import ExtendUserCreateForm
from users.models import ExtendUser


class UserCreateView(CreateView):
    template_name = 'users/signup.html'
    model = ExtendUser
    form_class = ExtendUserCreateForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        if self.request.method == 'POST':
            form = UserCreationForm(self.request.POST)
            if form.is_valid():
                user = form.save()
                return redirect('home')
        else:
            form = UserCreationForm()
