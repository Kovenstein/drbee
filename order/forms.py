from django import forms

from order.models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'address', 'phone', 'email']

    def __init__(self, *args, **kwargs):
        super(OrderForm, self). __init__(*args, **kwargs)
