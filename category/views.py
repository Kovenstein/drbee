from django.shortcuts import render, redirect

from cart.models import Cart
from product.models import Product


def cart(request, pk):
    Cart.object.create(user_id=request.user.id, product_id=pk)
    return redirect('products_per_category', Product.objects.get(id=pk).category_id)


def get_products_from_cart(request):
    return render(request, 'cart/cart_add_product.html', {'all_products': Cart.object.filter(user_id=request.user.id)})
