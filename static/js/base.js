const playIcon = document.querySelector(".play-button");
let audioPlay = false;
const playAudio = () => {
    const audio = document.querySelector("audio");
    if(!audioPlay){
        audio.play().then();
        audioPlay = true;
        playIcon.classList.remove('fa-play');
        playIcon.classList.add('fa-stop');
    }
    else {
        audio.pause();
        audioPlay = false;
        playIcon.classList.remove('fa-stop');
        playIcon.classList.add('fa-play');
    }
}

playIcon.addEventListener('click', playAudio);
