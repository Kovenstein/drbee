from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from rest_framework import viewsets

from beekeepers.forms import CategoryForm
from beekeepers.models import Beekeeper
from beekeepers.serializers import CategorySerializer


def beekeepers(request):
    return render(request, 'index_first.html')


class BeekeeperCreateView(CreateView):
    template_name = 'beekeeper/beekeeper_create.html'
    model = Beekeeper
    # fields = '__all__'
    success_url = reverse_lazy('create_new_beekeeper')
    form_class = CategoryForm


class BeekeeperListView(ListView):
    template_name = 'beekeeper/beekeeper_list.html'
    model = Beekeeper
    context_object_name = 'all_beekeepers'


class BeekeeperUpdateView(UpdateView):
    template_name = 'beekeeper/beekeeper_update.html'
    model = Beekeeper
    context_object_name = 'all_beekeeper'
    # fields = ['first_name', 'last_name', 'age']
    success_url = reverse_lazy('all_beekeepers')
    form_class = CategoryForm


class BeekeeperDeleteView(DeleteView):
    template_name = 'beekeeper/beekeeper_delete.html'
    model = Beekeeper
    context_object_name = 'beekeeper'
    success_url = reverse_lazy('all_beekeepers')


class BeekeeperDetailsView(DetailView):
    template_name = 'beekeeper/beekeeper_details.html'
    model = Beekeeper
    context_object_name = 'beekeeper'


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Beekeeper.objects.all()
    serializer_class = CategorySerializer
