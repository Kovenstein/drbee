from django.urls import path

from users import views

urlpatterns = [
    path('add_new_user', views.UserCreateView.as_view(), name='add_new_user')
]
